<?php
/**
 * NOTICE OF LICENSE.
 *
 * UNIT3D Community Edition is open-sourced software licensed under the GNU Affero General Public License v3.0
 * The details is bundled with this project in the file LICENSE.txt.
 *
 * @project    UNIT3D Community Edition
 *
 * @author     HDVinnie <hdinnovations@protonmail.com>
 * @license    https://www.gnu.org/licenses/agpl-3.0.en.html/ GNU Affero General Public License v3.0
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Download Check Page
    |--------------------------------------------------------------------------
    |
    | Weather Or Not User Will Be Stopped At Download Check Page Or Not
    |
    */

    'download_check_page' => 1,

    /*
    |--------------------------------------------------------------------------
    | Source Value
    |--------------------------------------------------------------------------
    |
    | Torrent Source Value
    |
    */

    'source' => 'Nocturnal',

    /*
    |--------------------------------------------------------------------------
    | Created By
    |--------------------------------------------------------------------------
    |
    | Created By Value
    |
    */

    'created_by'        => 'Created By Nocturnal',
    'created_by_append' => true,

    /*
    |--------------------------------------------------------------------------
    | Comment
    |--------------------------------------------------------------------------
    |
    | Comment Value
    |
    */

    'comment' => 'This torrent was downloaded from Nocturnal',

    /*
    |--------------------------------------------------------------------------
    | Magnet
    |--------------------------------------------------------------------------
    |
    | Enable/Disable magnet links
    |
    */

    'magnet' => 0,
];
