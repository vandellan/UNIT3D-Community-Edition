<?php
/**
 * NOTICE OF LICENSE.
 *
 * UNIT3D Community Edition is open-sourced software licensed under the GNU Affero General Public License v3.0
 * The details is bundled with this project in the file LICENSE.txt.
 *
 * @project    UNIT3D Community Edition
 *
 * @author     HDVinnie <hdinnovations@protonmail.com>
 * @license    https://www.gnu.org/licenses/agpl-3.0.en.html/ GNU Affero General Public License v3.0
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Welcome PM
    |--------------------------------------------------------------------------
    |
    | Subject
    |
    */

    'subject' => 'Welcome to Nocturnal',

    /*
    |--------------------------------------------------------------------------
    | Welcome PM
    |--------------------------------------------------------------------------
    |
    | Message
    |
    */

    'message' => 'Welcome To [b]Nocturnal[/b] Please be sure to read the FAQ and Rules of the tracker first - then drop on by the forum to introduce yourself.

    - Nocturnal City :space_invader:',
];
